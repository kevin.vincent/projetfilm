from django.shortcuts import render
from django.http import HttpResponseNotFound
from .models import Film


# Create your views here.

def home(self):
    topTenRating = Film.objects.order_by('-rating')[:10]
    lastTen = Film.objects.order_by('-releaseDate')[:10]
    return render(self, 'main_app/home.html', {'topTen': topTenRating, 'lastTen': lastTen})

def baseLayout(self):
    return render(self, 'baseLayout.html')

def not_found(request, exception):
    return render(request, template_name='errors/404.html')
