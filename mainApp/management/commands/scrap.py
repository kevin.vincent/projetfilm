from django.core.management.base import BaseCommand, CommandError

import requests
import json
import time
import re
from bs4 import BeautifulSoup

from mainApp.models import Country
from mainApp.models import Director
from mainApp.models import Film
from mainApp.models import Actor
from mainApp.models import Genre

class Command(BaseCommand):
    help = 'Script for movies data scrapping from themoviedb.org'

    def add_arguments(self, parser):
        parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):
        scraper()





def scraper():
    response = requests.get('https://www.themoviedb.org/discover/movie?language=fr&list_style=1&media_type=movie&page=1&primary_release_year=0&sort_by=popularity.desc&vote_count.gte=0')
    soup = BeautifulSoup(response.text, 'lxml')

    # print(soup.find(id='intro').text)
    selectYears = soup.find(id='year').text.split("\n")
    # selectYears
    # print(selectYears)
    moviePattern = re.compile(r'^[a-zA-Z0-9_]')
    nbpages = 0
    for year in selectYears:
        if year == '':
            continue
        if year == 'Aucun':
            continue
        
        responseYear = requests.get('https://www.themoviedb.org/discover/movie?language=fr&list_style=1&media_type=movie&page=1&primary_release_year='+str(year)+'&sort_by=popularity.desc&vote_count.gte=0')
        currSoup = BeautifulSoup(responseYear.text, 'lxml')
        currYearNbPagesA = currSoup.select(".pagination > a:nth-of-type(8)")
        # print (currYearNbPagesA[0].text)
        currYearNbPages = currYearNbPagesA[0].text
        nbpages += int(currYearNbPages)
        if int(year) <= 1950:
            break
        else:
            for i in range(0, int(currYearNbPages)):
                if i <= 10:
                    time.sleep(1)
                    responsePage = requests.get('https://www.themoviedb.org/discover/movie?language=fr&list_style=1&media_type=movie&page='+str(i)+'&primary_release_year='+str(year)+'&sort_by=popularity.desc&vote_count.gte=0')
                    currPageSoup = BeautifulSoup(responsePage.text, 'lxml')
                    results = currPageSoup.findAll("a", {"class": "title result"})
                    print("-------------------------------------")
                    print(year)
                    print("-------------------------------------")
                    for result in results:
                        validName = moviePattern.match(result.text)
                        if validName is not None:
                            currDetails = requests.get('https://www.themoviedb.org'+result['href'])
                            movieCreator(BeautifulSoup(currDetails.text, 'lxml'))
                            print ('https://www.themoviedb.org'+result['href'])
                else:
                    break
                
    print(nbpages)

def movieCreator(movieSoup):
    #movieName = movieSoup.findAll("")
    #find elems
    print(movieSoup)
    currMovie = Film(
        name = "",
        originalName = "",
        description = "",
        duration = "",
        public = "",
        releaseDate = "",
        rating = "",
        imgURL = ""
    )
    return currMovie


# print (results)
# for result in results:
#     print (result.text)

# TODO : 
# 1. Film ->
        # name
        # originalName
        # description
        # duration
        # public
        # releaseDate
        # rating
        # imgURL
# 2. Genres ->
#       name
#       film
# 2. Country ->  (+ push en base pour id + garder info) from actors & directors
#       libelle
# 3. Actor -> (+sieurs)
#       firstName 
        # lastName 
        # bornDate 
        # bio 
        # imgURL 
        # country -> set Country object
        # films -> set film object 
# 4. director ->
        # firstName
        # lastName
        # born_date
        # bio
        # country -> set Country object