import requests
import json
import time
import re
from bs4 import BeautifulSoup
import datetime


from mainApp.models import Country
from mainApp.models import Director
from mainApp.models import Film
from mainApp.models import Actor
from mainApp.models import Genre

Country.objects.all().delete()
Director.objects.all().delete()
Film.objects.all().delete()
Actor.objects.all().delete()
Genre.objects.all().delete()

# Ordre d'insertion :
# 1. Country
# 2. Director
# 3. Film
# 4. Actors

moviePattern = re.compile(r'^[a-zA-Z0-9_]')
releaseDatePattern = re.compile(r'\d.*\d')
datePattern = re.compile(r'^\d{4}-\d{2}-\d{2}$')
durationPattern = re.compile(r'^[0-9]h [0-9]{1,2}m$')
# name: link
actorLinks = {}
#identifiant: nomPays
countrys = {}

directorsList = {}


def scraper():
    response = requests.get('https://www.themoviedb.org/discover/movie?language=fr&list_style=1&media_type=movie&page=1&primary_release_year=0&sort_by=popularity.desc&vote_count.gte=0')
    soup = BeautifulSoup(response.text, 'lxml')

    selectYears = soup.find(id='year').text.split("\n")
    # selectYears
    # print(selectYears)
    nbpages = 0
    for year in selectYears:
        if year == '':
            continue
        if year == 'Aucun':
            continue

        responseYear = requests.get('https://www.themoviedb.org/discover/movie?language=fr&list_style=1&media_type=movie&page=1&primary_release_year='+str(year)+'&sort_by=popularity.desc&vote_count.gte=0')
        currSoup = BeautifulSoup(responseYear.text, 'lxml')
        currYearNbPagesA = currSoup.select(".pagination > a:nth-of-type(8)")
        currYearNbPages = currYearNbPagesA[0].text
        nbpages += int(currYearNbPages)
        if int(year) <= 2016:
            quit()
        else:
            for i in range(0, int(currYearNbPages)):
                if i <= 10:
                    time.sleep(1)
                    responsePage = requests.get('https://www.themoviedb.org/discover/movie?language=fr&list_style=1&media_type=movie&page='+str(i)+'&primary_release_year='+str(year)+'&sort_by=popularity.desc&vote_count.gte=0')
                    currPageSoup = BeautifulSoup(responsePage.text, 'lxml')
                    results = currPageSoup.findAll("a", {"class": "title result"})
                    print("-------------------------------------")
                    print(year)
                    print("-------------------------------------")
                    for result in results:
                        validName = moviePattern.match(result.text)
                        if validName is not None:
                            currDetails = requests.get('https://www.themoviedb.org'+result['href'])
                            print ('https://www.themoviedb.org'+result['href'])
                            movieSoup = BeautifulSoup(currDetails.text, 'lxml')

                            currMovie = movieCreator(movieSoup)
                            actorsli = movieSoup.findAll("li", {"class": "card"})
                            nbActorsSet = 0
                            for actorli in actorsli:
                                if nbActorsSet < 3:
                                    # trouver liens des pages des acteurs
                                    linksActor = actorli.findAll("a")
                                    #trouver le nom de l'acteur
                                    imgsActor = actorli.findAll("img")
                                    #print('Tableau ACTEUR : ' + imgsActor + ' fin tableau')
                                    if len(imgsActor) == 0:
                                        continue
                                    keyActorArray = imgsActor[0]["alt"]
                                    nbActorsSet = nbActorsSet+1
                                    if keyActorArray not in actorLinks:
                                        actorLinks[keyActorArray] = keyActorArray
                                        actorDetails = requests.get("https://www.themoviedb.org"+linksActor[0]['href'])
                                        actorSoup = BeautifulSoup(actorDetails.text, 'lxml')
                                        currActor = actorCreator(actorSoup, keyActorArray, currMovie)
                                    else:
                                        # juste ajouter le film a l'acteur
                                        names = ToolScrapActor.firstNameLastName(keyActorArray)
                                        firstName = names["firstName"]
                                        lastName = names["lastName"]
                                        currActor = Actor.objects.get(firstName = firstName, lastName= lastName)
                                        currActor.films.add(currMovie)
                            break
                else:
                    break

def countryCreator(code, name):
    if code not in countrys:
        countrys[code] = name
        currCountry = Country(
            code = code,
            libelle = name
        )
        currCountry.save()
    else:
        currCountry = Country.objects.get(code = code)
    return currCountry


def directorCreator(directorSoup, firstName, lastName):
    factsData = directorSoup.findAll("section" ,{"class": "facts left_column"})
    bornDate = ToolScrapActor.getBornDate(factsData[0])

    bio = ToolScrapActor.getBio(directorSoup)

    country = factsData[0].findAll("p")
    country = country[4].findAll(text=True, recursive=False)
    country = country[0].split(",")
    countryCode = country[-1][1:3]
    countryName = country[-1][1:]
    country = countryCreator(countryCode, countryName)
    pathImg = ToolScrapMovie.getImgURL(directorSoup)

    if datePattern.match(bornDate):
        bornDate = datetime.datetime.strptime(bornDate, '%Y-%m-%d').date()
    else :
        bornDate = None

    currDirector = Director(
        firstName = firstName,
        lastName = lastName,
        bornDate = bornDate,
        bio = bio,
        country = Country.objects.get(id= country.id),
        pathImg = pathImg
    )
    currDirector.save()
    return currDirector



def actorCreator(actorSoup, keyActorArray, currMovie):
    names = ToolScrapActor.firstNameLastName(keyActorArray)
    firstName = names["firstName"]
    lastName = names["lastName"]

    factsData = actorSoup.findAll("section" ,{"class": "facts left_column"})
    country = factsData[0].findAll("p")
    country = country[4].findAll(text=True, recursive=False)
    country = country[0].split(",")
    countryCode = country[-1][1:3]
    countryName = country[-1][1:]
    country = countryCreator(countryCode, countryName)

    bio = ToolScrapActor.getBio(actorSoup)
    pathImg = ToolScrapMovie.getImgURL(actorSoup)

    bornDate = ToolScrapActor.getBornDate(factsData[0])
    if datePattern.match(bornDate):
        bornDate = datetime.datetime.strptime(bornDate, '%Y-%m-%d').date()
    else :
        bornDate = None

    currActor = Actor(
        firstName = firstName,
        lastName = lastName,
        bornDate = bornDate,
        bio = bio,
        country = Country.objects.get(id= country.id),
        pathImg = pathImg,
    )
    currActor.save()
    currActor.films.add(currMovie)
    return currActor


def movieCreator(movieSoup):
    someMovieData = movieSoup.findAll("section", {"class": "facts"})

    currDirectorPath = movieSoup.findAll("ol", {"class": "people no_image"})
    currDirectorPath = currDirectorPath[0].findAll("a")
    if len(currDirectorPath) > 4:
        currDirectorName = currDirectorPath[4].text
        currDirectorNameSplited = currDirectorPath[4].text.split(" ")
        firstName = currDirectorNameSplited[0]
        lastName = currDirectorNameSplited[1]
        if currDirectorName not in directorsList:
            directorDetails = requests.get("https://www.themoviedb.org"+ currDirectorPath[4]['href'])
            directorSoup = BeautifulSoup(directorDetails.text, 'lxml')
            currDirector = directorCreator(directorSoup, firstName, lastName)
            directorsList[currDirectorName] = directorDetails
        else :
            currDirector = Director.objects.filter(firstname = firstname, lastName = lastName)
    else :
        currDirector = None

    currMovie = Film(
        name = ToolScrapMovie.getTitle(movieSoup),
        originalName = ToolScrapMovie.getOriginalName(someMovieData),
        description =  ToolScrapMovie.getDescription(movieSoup),
        duration = ToolScrapMovie.getDuration(someMovieData),
        releaseDate = ToolScrapMovie.getReleaseDate(someMovieData),
        rating = ToolScrapMovie.getRating(movieSoup),
        pathImg = ToolScrapMovie.getImgURL(movieSoup),
        director = currDirector
    )
    currMovie.save()
    return currMovie

class ToolScrapMovie():
    def getTitle(movieSoup):
        movieName = movieSoup.findAll("div", {"class": "title"})
        movieTitle = movieName[0].findAll("h2")
        return movieTitle[0].text

    def getDescription(movieSoup):
        movieDescription = movieSoup.findAll("div", {"class": "overview"})
        return movieDescription[0].text

    def getDuration(someMovieData):
        movieData = someMovieData[0].find_all("p")
        movieData = movieData[5].findAll(text=True, recursive=False)
        movieData = movieData[0][1:]
        if not durationPattern.match(movieData):
            movieData = None
        return movieData

    def getReleaseDate(someMovieData):
        movieReleaseDateData = someMovieData[0].find_all("ul")
        movieReleaseDateText = movieReleaseDateData[0].li.findAll(text=True, recursive=False)

        movieReleaseDate = ""
        for text in movieReleaseDateText:
            date = releaseDatePattern.findall(text)
            if date:
                movieReleaseDate = date[0]
                break

        return movieReleaseDate

    def getOriginalName(someMovieData):
        originalNameP = someMovieData[0].find_all("p", {"class": "wrap"})
        if (originalNameP == []):
            return ""
        else:
            arrayOriginalNameP = originalNameP[0].findAll(text=True, recursive=False)
            return arrayOriginalNameP[0]


    def getImgURL(movieSoup):
        urlArray = movieSoup.findAll("img", {"class": "poster"})
        # url = urlArray[0].findAll(src=True, recursive=False)
        url = ""
        if len(urlArray) > 0:
            url = urlArray[0]['src']
        return url

    def getRating(movieSoup):
        ratingDivs = movieSoup.findAll("div", {"class": "user_score_chart"})
        return ratingDivs[0]["data-percent"]

    def getPublic():
        return ''


class ToolScrapActor():
    def firstNameLastName(fullname):
        names = fullname.split(" ")
        lastName = ""
        if len(names) == 2:
            lastName = names[1]

        return {"firstName": names[0], "lastName": names[1]}

    def getBornDate(factsData):
        factsP = factsData.findAll("p")
        borndate = factsP[3].findAll(text=True, recursive=False)
        borndate = borndate[0]
        return borndate[1:]

    def getBio(actorSoup):
        arrayBio = actorSoup.findAll("div", {"class": "text"})
        bio = ""
        if len(arrayBio) != 0:
            texts = arrayBio[0].findAll("p")
            for txt in texts:
                bio = bio+txt.text
        return bio

    def getMovies():
        return country

scraper()

logic.py
