from mainApp.models import Film
import django_filters

class FilmFilter(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')
    class Meta:
        model = Film
        fields = ['name']
