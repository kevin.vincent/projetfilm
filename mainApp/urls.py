from django.contrib import admin
from django.urls import path

from .views import film
from .views import actor
from .views import director
from .views import country
from . import main_views

urlpatterns = [
    path('', main_views.home),

    # baseLayout test route
    path('baseLayout/', main_views.baseLayout),

    # film routes
    path('film/', film.getList),
    path('film/add/', film.add),
    path('film/update/<id>', film.update),
    path('film/delete/<id>', film.delete),
    path('film/details/<id>', film.details),

    # actor routes
    path('actor/', actor.getList),
    path('actor/add/', actor.add),
    path('actor/update/<id>', actor.update),
    path('actor/delete/<id>', actor.delete),
    path('actor/details/<id>', actor.details),

    # director routes
    path('director/', director.getList),
    path('director/add/', director.add),
    path('director/update/<id>', director.update),
    path('director/delete/<id>', director.delete),
    path('director/details/<id>', director.details),

    # country routes
    path('country/', country.getList),
    path('country/add/', country.add),
    path('country/delete/<id>', country.delete),
    path('country/details/<id>', country.details),
]
