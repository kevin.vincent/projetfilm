from django.test import TestCase, Client
from django.http import HttpRequest
from django.http import QueryDict
from ..views import film
import time
from datetime import date
from ..models import Film
from ..models import Director
from django.core.exceptions import ObjectDoesNotExist

class FilmTest(TestCase):
    def test_film_add(self):
        c = Client()
        customName = str(time.time())
        # director = Director.objects.order_by('id')[:1]
        # director = director[0].id
        c.post('/main/film/add/', {'name=': customName, 'originalName': 'test', 'pathImg': 'test', 'description': 'test', 'duration': 'test', 'public': 'test', 'releaseDate': str(date(2018, 10, 25)), 'rating': '50', 'director': '1'})


        try:
            newFilm = Film.objects.get(name=customName)
            self.assertEqual(1, 1)
        except ObjectDoesNotExist:
            self.assertEqual(customName, 2)
