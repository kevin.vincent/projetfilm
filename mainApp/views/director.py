from django.shortcuts import render
from django.http import HttpResponse
from ..models import Director
from django.forms import ModelForm
from ..models import Film
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator

class DirectorForm(ModelForm):
    class Meta:
        model = Director
        fields = ('firstName', 'lastName', 'bornDate', 'bio', 'country', 'pathImg')

topTenFilm = Film.objects.order_by('-rating')[:10]
lastTenFilm = Film.objects.order_by('-releaseDate')[:10]

def add(request):
    form = DirectorForm()
    if request.method == "POST":
        form = DirectorForm(request.POST)
        if form.is_valid():
            new_director = form.save()
            return render(request, template_name='director/list_display.html', context={'director': new_director} )
    context = {'form': form, 'topTen': topTenFilm, 'lastTen': lastTenFilm}
    return render(request,'director/director_form.html', context)

def update(request, id):
    director = Director.objects.get(pk=id)
    if request.method == "POST":
        form = DirectorForm(request.POST, instance=director)
        if form.is_valid():
            form.save()
            return render(request, template_name='main_app/index_ex.html', context={'director': id} )
    form = DirectorForm(instance=director)
    context = {'form': form, 'topTen': topTenFilm, 'lastTen': lastTenFilm}
    return render(request,'director/director_form.html', context)

def delete(self, id):
    director = Director.objects.get(pk=id)
    director.delete()
    return HttpResponse('delete director') #TODO define delete response

def details(request, id):
    director = Director.objects.get(pk=id)
    return render(request, template_name='director/director.html', context={'director': director, 'topTen': topTenFilm, 'lastTen': lastTenFilm} )

def getList(request):
    director_list = Director.objects.all()
    paginator = Paginator(director_list, 10)

    page = request.GET.get('page')
    directors = paginator.get_page(page)
    return render(request, template_name='director/list_display.html', context={'objects': directors, 'topTen': topTenFilm, 'lastTen': lastTenFilm} )
