from django.shortcuts import render
from django.http import HttpResponse
from ..models import Film
from django.forms import ModelForm
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from ..filmFilter import FilmFilter

class FilmForm(ModelForm):
    class Meta:
        model = Film
        fields = ('name', 'originalName',
        'description', 'duration', 'public',
        'releaseDate', 'rating', 'director', 'pathImg')

topTenFilm = Film.objects.order_by('-rating')[:10]
lastTenFilm = Film.objects.order_by('-releaseDate')[:10]

def add(request):
    form = FilmForm()
    if request.method == "POST":
        form = FilmForm(request.POST)
        if form.is_valid():
            new_film = form.save()
            film = new_film
            actors = film.actor_set.all()
            return render(request, template_name='film/film.html', context={'film': film, 'actors': actors, 'topTen': topTenFilm, 'lastTen': lastTenFilm} )

    context = {'form': form, 'topTen': topTenFilm, 'lastTen': lastTenFilm}
    return render(request,'film/film_form.html', context)

def update(request, id):
    film = Film.objects.get(pk=id)
    if request.method == "POST":
        form = FilmForm(request.POST, instance=film)
        if form.is_valid():
            form.save()
            return render(request, template_name='film/list_display.html')
    form = FilmForm(instance=film)
    context = {'form': form, 'topTen': topTenFilm, 'lastTen': lastTenFilm}
    return render(request,'film/film_form.html', context)

def delete(request, id):
    film = Film.objects.get(pk=id)
    film.delete()
    return HttpResponse('delete film') #TODO define delete response

def details(request, id):
    film = Film.objects.get(pk=id)
    actors = film.actor_set.all()
    return render(request, template_name='film/film.html', context={'film': film, 'actors': actors, 'topTen': topTenFilm, 'lastTen': lastTenFilm} )

def getList(request):
    filmList = []

    resFilter = filterFilmsByName(request)
    filmList = resFilter[0]
    searchMsg = resFilter[1]
    searchTerm = resFilter[2]

    if len(filmList) == 0:
        filmList = Film.objects.all()

    paginator = Paginator(filmList, 10)
    page = request.GET.get('page')

    films = paginator.get_page(page)
    return render(request, template_name='film/list_display.html', context={'objects': films, 'topTen': topTenFilm, 'lastTen': lastTenFilm, 'searchMsg' : searchMsg, 'searchTerm': searchTerm} )

def filterFilmsByName(rq):
    filteredList = []
    message = ""
    if rq.GET.get('name'):
        filteredList = Film.objects.filter(name__contains=rq.GET.get('name'))
        if len(filteredList) == 0:
            message = "No movies were found for this name"
    return [filteredList, message, rq.GET.get('name')]
