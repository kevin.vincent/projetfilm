from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.forms import ModelForm
from ..models import Country
from django.urls import reverse

class CountryForm(ModelForm):
    class Meta:
        model = Country
        fields = ('code', 'libelle')

def add(request):
    form = CountryForm()
    if request.method == "POST":
        form = CountryForm(request.POST)
        if form.is_valid():
            new_country = form.save()
            return render(request, template_name='country/list_display.html', context={'country': new_country} )
    context = {'form': form}
    return render(request,'country/country_form.html', context)

def update(self):
    return HttpResponse('update country')

def delete(self, id):
    country = Country.objects.get(pk=id)
    country.delete()
    return HttpResponse('delete country') #TODO define delete response

def details(self, id):
    country = Country.objects.get(pk=id)
    return render(self, template_name='country/country.html', context={'country': country} )

def getList(request):
    countries = Country.objects.all().order_by('id')
    return render(request, template_name='country/list_display.html', context={'objects': countries} )
