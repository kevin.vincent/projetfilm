from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.forms import ModelForm
from ..models import Actor
from django.urls import reverse
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from ..models import Film

class ActorForm(ModelForm):
    class Meta:
        model = Actor
        fields = ('firstName', 'lastName', 'bornDate', 'bio', 'country', 'films', 'pathImg')

topTenFilm = Film.objects.order_by('-rating')[:10]
lastTenFilm = Film.objects.order_by('-releaseDate')[:10]

def add(request):
    form = ActorForm()
    if request.method == "POST":
        form = ActorForm(request.POST)
        if form.is_valid():
            new_actor = form.save()
            return render(request, template_name='actor/list_display.html', context={'actor': new_actor} )
    context = {'form': form, 'topTen': topTenFilm, 'lastTen': lastTenFilm}
    return render(request,'actor/actor_form.html', context)

def update(request, id):
    actor = Actor.objects.get(pk=id)
    if request.method == "POST":
        form = ActorForm(request.POST, instance=actor)
        if form.is_valid():
            form.save()
            return render(request, template_name='main_app/index_ex.html', context={'actor': id} )
    form = ActorForm(instance=actor)
    context = {'form': form, 'topTen': topTenFilm, 'lastTen': lastTenFilm}
    return render(request,'actor/actor_form.html', context)

def delete(request, id):
    actor = Actor.objects.get(pk=id)
    actor.delete()
    return HttpResponse('delete actor') #TODO define delete response

def details(request, id):
    actor = Actor.objects.get(pk=id)
    films = actor.films.all()
    return render(request, template_name='actor/actor.html', context={'actor': actor, 'films': films, 'topTen': topTenFilm, 'lastTen': lastTenFilm} )

def getList(request):
    actor_list = Actor.objects.all()
    paginator = Paginator(actor_list, 10)

    page = request.GET.get('page')
    actors = paginator.get_page(page)
    return render(request, template_name='actor/list_display.html', context={'objects': actors, 'topTen': topTenFilm, 'lastTen': lastTenFilm} )
