from django.db import models

class Country(models.Model):
    code = models.CharField(max_length=25)
    libelle = models.CharField(max_length=50)

    def __str__(self):
        return self.libelle

class Director(models.Model):
    firstName = models.CharField(max_length=250)
    lastName = models.CharField(max_length=250, null=True)
    bornDate = models.DateField(null=True)
    bio = models.TextField()
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    pathImg = models.TextField(null=True)

    def __str__(self):
        return self.firstName

class Film(models.Model):
    name = models.CharField(max_length=250)
    originalName = models.CharField(max_length=250, null=True)
    description = models.TextField()
    duration = models.CharField(max_length=250, null=True)
    public = models.CharField(max_length=250, null=True)
    releaseDate = models.CharField(max_length=250, null=True)
    rating = models.FloatField(null=True)
    director = models.ForeignKey(Director, on_delete=models.CASCADE, null=True)
    pathImg = models.TextField(null=True)

    def __str__(self):
        return self.name

class Actor(models.Model):
    firstName = models.CharField(max_length=250)
    lastName = models.CharField(max_length=250, null=True)
    bornDate = models.DateField(null=True)
    bio = models.TextField()
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    films = models.ManyToManyField(Film)
    pathImg = models.TextField(null=True)

    def __str__(self):
        return self.firstName

class Genre(models.Model):
    name = models.CharField(max_length=250)
    films = models.ManyToManyField(Film)
