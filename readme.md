# Pré-requis :

 - git
 - python 3.6.7
 - pip

# Instructions :

 - ouvrir terminal
 - git clone -b clean https://gitlab.com/kevin.vincent/projetfilm.git
 - cd projetFilm
 - pip3 install -r requirements.txt
 - python3 manage.py makemigrations
 - python3 manage.py migrate
 - python3 manage.py logic
 - python3 manage.py runserver
 - ouvrir navigateur -> http://localhost:8000/main
